module.exports = function (grunt) {
  'use strict';

  var DEFAULT_HOST = 'microtechnology.cumulocity.com',
    DEFAULT_PROTOCOL = 'http',
    host = DEFAULT_HOST,
    protocol = DEFAULT_PROTOCOL;

  if (grunt.option('host')) {
    host = grunt.option('host');
    if (host === 'developer') {
      host = 'developer.cumulocity.com';
    }

    if (host === 'developer.cumulocity.com') {
      protocol = 'https';
    }
  }


  protocol = 'https';
  //protocol = grunt.option('protocol') || protocol;
  grunt.config('cumulocity.host', host);
  grunt.config('cumulocity.protocol', protocol);
  if (grunt.option('port')) {
    grunt.config('cumulocity.port', Number(grunt.option('port')));
  }

  grunt.config('paths.root', './');
  grunt.config('paths.temp', '.tmp');
  grunt.config('paths.build', 'build');
  grunt.config('paths.plugins', 'plugins');
  grunt.config('paths.bower', 'bower_components');

  //Load cumulocity grunt tasks
  grunt.loadNpmTasks('grunt-cumulocity-ui-tasks');

  grunt.registerTask('server', [
    'pluginPreAll',
    'connect:server',
    'watch'
  ]);

  grunt.registerTask('build', [
    'pluginBuildAll'
  ]);

  /**
   * grunt-karma task
   */
  grunt.loadNpmTasks('grunt-karma');
  grunt.config('karma', {
    test: {
      options: {
        configFile: 'karma.conf.js',
        files: []
      }
    }
  });
  grunt.config('specFiles', [
    'plugins/**/*spec.js'
  ]);

  grunt.registerTask('test', [
    'core-config',
    'prepareTest',
    'karma'
  ]);



};