(function () {

  angular.module('myplugin', [])
    .config(['c8yNavigatorProvider', '$routeProvider', config]);

  function config(c8yNavigatorProvider, $routeProvider) {

    c8yNavigatorProvider.addNavigation({
      priority: 10000,
      name: 'Home',
      icon: 'home',
      path: ''
    });

    c8yNavigatorProvider.addNavigation({
      priority: 100,
      name: 'Charts demo',
      icon: 'line-chart',
      path: 'charts'
    });

    $routeProvider.when('/', {
      templateUrl: ':::PLUGIN_PATH:::/views/home.html'
    });

    $routeProvider.when('/charts', {
      templateUrl: ':::PLUGIN_PATH:::/views/charts.html'
    });

  }

})();