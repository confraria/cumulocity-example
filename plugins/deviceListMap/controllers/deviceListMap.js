angular.module('c8y.parts.deviceListMap')
.controller('deviceListMapVPCtrl',
['$scope', 'c8yDevices', 'c8yInventory', 'c8yGeo', 'leafletBoundsHelpers', 'c8yTitle', 'c8yRealtime',
function ($scope, c8yDevices, c8yInventory, c8yGeo, leafletBoundsHelpers, c8yTitle, c8yRealtime) {
  'use strict';

  var C8Y_POSITION = 'c8y_Position',
    V4E_POSITION = 'com_nsn_startups_vendme_fragments_GPSCoordinates',
    initialRealtimeState = false,
    ZOOM = 11,
    fragment = C8Y_POSITION,
    filter = {
      fragmentType: fragment
    },
    markers = [],
    bounds = new c8yGeo.Bounds(),
    centerPoint = {
      lat: 0,
      lng: 0,
      zoom: ZOOM
    };

  function onMarkers(_markers) {
    markers.length = 0;
    _markers.forEach(function (marker) {
      markers.push(marker);
    });
  }

  function onMarkersChange(_markers) {
    var hasMarkers = _markers && _markers.length;

    setTitle(hasMarkers);

    if (!hasMarkers) {
      return;
    }

    bounds.reset();
    _markers.forEach(angular.bind(bounds, bounds.add));
    $scope.bounds = bounds.get();
  }

  function makeMessage(mo) {
    var list = [
      '<a class="btn btn-link" href="#/device/',
      mo.id,
      '">',
      mo.name,
      '</a>'
    ];
    return list.join('');
  }

  function managedObjectsToMarkers(mos) {
    var result = [];
    mos.forEach(function (mo) {
      var frag = mo[fragment],
        marker = angular.copy(frag || {});

      marker.lat = frag.lat = Number(frag.lat);
      marker.lng = frag.lng = Number(frag.lng);
      
      if (frag &&
        angular.isNumber(frag.lat) &&
        angular.isNumber(frag.lng) &&
        !isNaN(frag.lat) &&
        !isNaN(frag.lng)) {
				
				marker.id = mo.id;
				marker.message = makeMessage(mo);
				result.push(marker);
      }
    });
    return result;
  }

  function getManagedObjects() {
    c8yInventory.list(filter)
      .then(managedObjectsToMarkers)
      .then(onMarkers);
  }

  function setTitle(qty) {
    c8yTitle.changeTitle({
      title: 'Device Map',
      subtitle: qty ? qty + ' Markers' : undefined
    });
  }
  
  function setFragment(frag) {
    fragment = frag;
    filter.fragmentType = fragment;
  }

  function checkVendme() {
    var _filter = {
      fragmentType: V4E_POSITION,
      pageSize: 1
    };
    c8yInventory.list(_filter)
      .then(function (items) {
        if (items.length) {
          setFragment(V4E_POSITION);
          getManagedObjects();
        }
      });
  }

  function createListener(evt, data) {
		for (var i in markers) {
			if (markers[i].id === data.id) {
				return;
			}
		}
		var marker = managedObjectsToMarkers([data]);
		if (marker.length > 0) {
			markers.push(marker[0]);
		}
  }
  
  function updateListener(evt, data) {
		var frag = data[fragment];
		if (frag) {
			for (var i in markers) {
				if (markers[i].id === data.id) {
					markers[i] = angular.extend({}, markers[i], frag);
					return;
				}
			}
			var marker = managedObjectsToMarkers([data]);
			if (marker.length > 0) {
				markers.push(marker[0]);
			}
		}
  }
  
  function subscribeListener() {
		checkVendme();
		getManagedObjects();
		setTitle();
  }
  
  function initPlugin() {
		if (initialRealtimeState) {
			$scope.switchRealtime();
		} else {
			checkVendme();
			getManagedObjects();
			setTitle();
		}
  }

  $scope.markers = markers;
  $scope.bounds = leafletBoundsHelpers.createBoundsFromArray([[0, 0], [0, 0]]);
  $scope.$watchCollection('markers', onMarkersChange);
  $scope.mapDefault = {scrollWheelZoom: false};
  $scope.center = centerPoint;
  $scope.paging = {refresh: true};
  $scope.refresh = getManagedObjects;
  $scope.realtimeChannel = '/managedobjects/*';
  
  c8yRealtime.addListener($scope.$id, $scope.realtimeChannel, $scope.$id+'-subscribed', subscribeListener);
  c8yRealtime.addListener($scope.$id, $scope.realtimeChannel, 'CREATE', createListener);
  c8yRealtime.addListener($scope.$id, $scope.realtimeChannel, 'UPDATE', updateListener);
  
  initPlugin();

}]);
