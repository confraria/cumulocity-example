angular.module('c8y.parts.deviceListMap', ['leaflet-directive'])
.config(['$routeProvider', 'c8yNavigatorProvider',
function ($routeProvider, c8yNavigatorProvider) {
  'use strict';

  c8yNavigatorProvider.addNavigation({
    parent: 'Devices',
    name: 'Map',
    priority: 1900,
    path: 'devicemap',
    icon: 'map-marker'
  });

  $routeProvider.when('/devicemap', {
    templateUrl: ':::PLUGIN_PATH:::/views/index.html',
  });
}]);
